CREATE OR REPLACE TYPE t_payment_detail IS OBJECT
(
  field_id    NUMBER(10),
  field_value VARCHAR2(200 CHAR)
)
;
/
