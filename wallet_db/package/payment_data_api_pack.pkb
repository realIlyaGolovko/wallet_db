CREATE OR REPLACE PACKAGE BODY payment_detail_api_pack IS

  --признак, выполняется ли изменение через API
  g_is_api BOOLEAN := FALSE;

  --разрешение изменять данные
  PROCEDURE allow_changes IS
  BEGIN
    g_is_api := TRUE;
  END allow_changes;

  --запрет изменять данные
  PROCEDURE disallow_changes IS
  BEGIN
    g_is_api := FALSE;
  END disallow_changes;

  --Вставка/изменение деталей платежа
  PROCEDURE insert_or_update_payment_detail
  (
    p_payment_id     payment.payment_id%TYPE
   ,p_payment_detail t_payment_detail_array
  ) IS
  
  BEGIN
    IF p_payment_id IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_object_id);
    END IF;
    IF p_payment_detail IS NOT empty
    THEN
      FOR detail IN p_payment_detail.first .. p_payment_detail.last
      LOOP
        IF (p_payment_detail(detail).field_id IS NULL)
        THEN
          raise_application_error(common_pack.c_error_code_invalid_input_parameter
                                 ,common_pack.c_error_msg_field_id);
        END IF;
      
        IF (p_payment_detail(detail).field_value IS NULL)
        THEN
          raise_application_error(common_pack.c_error_code_invalid_input_parameter
                                 ,common_pack.c_error_msg_filed_value);
        END IF;
      END LOOP;
    ELSE
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_collection_is_empty);
    END IF;
    payment_api_pack.try_lock_payment(p_payment_id => p_payment_id);
    allow_changes();
    --Вставка/изменение данных
    MERGE INTO payment_detail pd
    USING (SELECT p_payment_id payment_id
                 ,VALUE       (vpd).field_id        field_id
                 ,VALUE       (vpd).field_value        field_value
             FROM TABLE(p_payment_detail) vpd) t
    ON (pd.payment_id = t.payment_id AND pd.field_id = t.field_id)
    WHEN MATCHED THEN
      UPDATE SET pd.field_value = t.field_value
    WHEN NOT MATCHED THEN
      INSERT (payment_id, field_id, field_value) VALUES (t.payment_id, t.field_id, t.field_value);
    disallow_changes();
  EXCEPTION
    WHEN OTHERS THEN
      disallow_changes();
      RAISE;
  END insert_or_update_payment_detail;

  --Удаление деталей платежа
  PROCEDURE delete_payment_detail
  (
    p_payment_id       payment.payment_id%TYPE
   ,p_delete_field_ids t_number_array
  ) IS
  BEGIN
    IF p_payment_id IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_object_id);
    END IF;
    IF p_delete_field_ids IS NOT empty
    THEN
      payment_api_pack.try_lock_payment(p_payment_id => p_payment_id);
      allow_changes();
      --удаление данных
      DELETE payment_detail pd
       WHERE pd.payment_id = p_payment_id
         AND pd.field_id IN (SELECT VALUE(vdf) FROM TABLE(p_delete_field_ids) vdf);
    ELSE
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_collection_is_empty);
      disallow_changes();
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      disallow_changes();
      RAISE;
  END delete_payment_detail;

  --Проверка, вызываемая из тригера
  PROCEDURE is_changes_through_api IS
  BEGIN
    IF NOT g_is_api
       AND NOT common_pack.is_manual_changes_allowed
    THEN
      raise_application_error(common_pack.c_error_code_manual_changes
                             ,common_pack.c_error_msg_manual_changes);
    END IF;
  END is_changes_through_api;

END;
/