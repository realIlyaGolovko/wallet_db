CREATE OR REPLACE PACKAGE payment_api_pack IS
  /*
  �����: ������� �.�.
  ��������: API ��� �������� �������
  */

  c_payment_status_created   CONSTANT payment.status%TYPE := 0;
  c_payment_status_completed CONSTANT payment.status%TYPE := 1;
  c_payment_status_error     CONSTANT payment.status%TYPE := 2;
  c_payment_status_canceled  CONSTANT payment.status%TYPE := 3;

  --�������� �������
  FUNCTION create_payment
  (
    p_payment_detail t_payment_detail_array
   ,p_from_client_id client.client_id%TYPE
   ,p_to_client_id   client.client_id%TYPE
   ,p_summa          payment.summa%TYPE
   ,p_currency_id    currency.currency_id%TYPE
   ,p_date           payment.create_dtime%TYPE
  ) RETURN payment.payment_id%TYPE;

  --����� ������� � "��������� ������" � ��������� �������
  PROCEDURE fail_payment
  (
    p_payment_id    payment.payment_id%TYPE
   ,p_status_reason payment.status_change_reason%TYPE
   ,p_date          payment.create_dtime%TYPE
  );

  --������ ������� � ��������� �������
  PROCEDURE cancel_payment
  (
    p_payment_id    payment.payment_id%TYPE
   ,p_status_reason payment.status_change_reason%TYPE
   ,p_date          payment.create_dtime%TYPE
  );

  --������� ������� � "�������� ������"
  PROCEDURE successful_finish_payment
  (
    p_payment_id payment.payment_id%TYPE
   ,p_date       payment.create_dtime%TYPE
  );

  --���������� ������� ��� ���������
  PROCEDURE try_lock_payment(p_payment_id payment.payment_id%TYPE);

  --��������

  --����������� �� ��������� ����� API
  PROCEDURE is_changes_through_api;

  --�������� �� ����������� ������� ������
  PROCEDURE check_payment_delete_restriction;
END;
/
