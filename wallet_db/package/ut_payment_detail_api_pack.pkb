CREATE OR REPLACE PACKAGE BODY ut_payment_detail_api_pack IS

  g_payment_id           payment.payment_id%TYPE;
  g_payment_filed_value  payment_detail.field_value%TYPE;
  g_field_id             payment_detail.field_id%TYPE;
  g_payment_detail_array t_payment_detail_array;
  g_delete_field_ids     t_number_array;

  --инициализация дефолтного платежа
  PROCEDURE setup_default_payment IS
  BEGIN
    g_payment_id := ut_common_pack.create_default_payment;
  END setup_default_payment;

  PROCEDURE insert_payment_detail_create_new_details IS
  BEGIN
    --arrange
    --создание платежа с одной деталью
    g_payment_detail_array := ut_common_pack.get_random_payment_detail_array(p_detail_amount => 1);
    g_payment_id           := ut_common_pack.create_default_payment(p_payment_detail => g_payment_detail_array);
    --подготовка данных для созадния платежа с двумя деталями 
    g_payment_detail_array := ut_common_pack.get_random_payment_detail_array(p_detail_amount => 2);
    --act
    payment_detail_api_pack.insert_or_update_payment_detail(p_payment_id     => g_payment_id
                                                           ,p_payment_detail => g_payment_detail_array);
    g_payment_filed_value := ut_common_pack.get_payment_info_field_value(p_payment_id => g_payment_id
                                                                        ,p_field_id   => g_payment_detail_array(2).field_id);
    --assert
    ut3.ut.expect(g_payment_filed_value
                 ,'Значение поля field_value не соответствует ожидаемому').to_equal(g_payment_detail_array(2).field_value);
  END insert_payment_detail_create_new_details;

  PROCEDURE update_payment_detail_change_details IS
  BEGIN
    --arrange  
    g_payment_detail_array := ut_common_pack.get_random_payment_detail_array(p_detail_amount => 1);
    g_field_id             := g_payment_detail_array(1).field_id;
    g_payment_id           := ut_common_pack.create_default_payment(p_payment_detail => g_payment_detail_array);
    --изменяю поле field_value
    g_payment_detail_array(1).field_value := dbms_random.string('u', 10);
    --act                                                                                                                                                                        
    payment_detail_api_pack.insert_or_update_payment_detail(p_payment_id     => g_payment_id
                                                           ,p_payment_detail => g_payment_detail_array);
    g_payment_filed_value := ut_common_pack.get_payment_info_field_value(p_payment_id => g_payment_id
                                                                        ,p_field_id   => g_field_id);
    --assert
    ut3.ut.expect(g_payment_filed_value
                 ,'Значение поля field_value не соответствует ожидаемому').to_equal(g_payment_detail_array(1).field_value);
  END update_payment_detail_change_details;

  PROCEDURE delete_payment_detail_delete_details IS
    v_payment_filed_value payment_detail.field_value%TYPE;
    v_first_id            payment_detail.payment_id%TYPE;
    v_last_id             payment_detail.payment_id%TYPE;
  BEGIN
    --arrange
    g_payment_detail_array := ut_common_pack.get_random_payment_detail_array(2);
    v_first_id             := g_payment_detail_array(g_payment_detail_array.first).field_id;
    v_last_id              := g_payment_detail_array(g_payment_detail_array.last).field_id;
    g_payment_id           := ut_common_pack.create_default_payment(p_payment_detail => g_payment_detail_array);
    g_delete_field_ids     := t_number_array(v_first_id, v_last_id);
    --act
    payment_detail_api_pack.delete_payment_detail(p_payment_id       => g_payment_id
                                                 ,p_delete_field_ids => g_delete_field_ids);
    g_payment_filed_value := ut_common_pack.get_payment_info_field_value(p_payment_id => g_payment_id
                                                                        ,p_field_id   => v_first_id);
    v_payment_filed_value := ut_common_pack.get_payment_info_field_value(p_payment_id => g_payment_id
                                                                        ,p_field_id   => v_last_id);
    --assert
    ut3.ut.expect(g_payment_filed_value
                 ,'Значение filed_value для удаляемых деталей должно быть null').to_be_null;
    ut3.ut.expect(v_payment_filed_value
                 ,'Значение filed_value для удаляемых деталей должно быть null').to_be_null;
  END delete_payment_detail_delete_details;

  PROCEDURE error_when_insert_or_update_detail_with_input_invalid_details IS
  BEGIN
    --arrange
    g_payment_detail_array := NULL;
    --act
    payment_detail_api_pack.insert_or_update_payment_detail(p_payment_id     => g_payment_id
                                                           ,p_payment_detail => g_payment_detail_array);
  END error_when_insert_or_update_detail_with_input_invalid_details;

  PROCEDURE error_when_delete_payment_detail_with_input_invalid_payment_id IS
  BEGIN
    --arrange
    g_payment_id       := NULL;
    g_delete_field_ids := t_number_array(1, 2);
    --act
    payment_detail_api_pack.delete_payment_detail(p_payment_id       => g_payment_id
                                                 ,p_delete_field_ids => g_delete_field_ids);
  END error_when_delete_payment_detail_with_input_invalid_payment_id;

  PROCEDURE erro_when_delete_payment_detail_with_input_not_init_collection IS
    v_delete_field_ids t_number_array;
  BEGIN
    --act
    payment_detail_api_pack.delete_payment_detail(p_payment_id       => g_payment_id
                                                 ,p_delete_field_ids => v_delete_field_ids);
  END erro_when_delete_payment_detail_with_input_not_init_collection;

  PROCEDURE erro_when_delete_payment_detail_with_input_empty_collection IS
  BEGIN
    --arrange
    g_delete_field_ids := t_number_array();
    --act
    payment_detail_api_pack.delete_payment_detail(p_payment_id       => g_payment_id
                                                 ,p_delete_field_ids => g_delete_field_ids);
  END erro_when_delete_payment_detail_with_input_empty_collection;

END ut_payment_detail_api_pack;
/