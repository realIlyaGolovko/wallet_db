CREATE OR REPLACE PACKAGE BODY payment_api_pack IS

  --признак, выполняется ли изменение через API
  g_is_api BOOLEAN := FALSE;

  --разрешение изменять данные
  PROCEDURE allow_changes IS
  BEGIN
    g_is_api := TRUE;
  END allow_changes;

  --запрет изменять данные
  PROCEDURE disallow_changes IS
  BEGIN
    g_is_api := FALSE;
  END disallow_changes;

  --Создание платежа
  FUNCTION create_payment
  (
    p_payment_detail t_payment_detail_array
   ,p_from_client_id client.client_id%TYPE
   ,p_to_client_id   client.client_id%TYPE
   ,p_summa          payment.summa%TYPE
   ,p_currency_id    currency.currency_id%TYPE
   ,p_date           payment.create_dtime%TYPE
  ) RETURN payment.payment_id%TYPE IS
    v_message    VARCHAR2(200 CHAR) := 'Платеж создан';
    v_payment_id payment.payment_id%TYPE;
  BEGIN
    allow_changes();
    --создание платежа
    INSERT INTO payment
      (payment_id
      ,create_dtime
      ,summa
      ,currency_id
      ,from_client_id
      ,to_client_id
      ,status
      ,status_change_reason)
    VALUES
      (payment_seq.nextval
      ,p_date
      ,p_summa
      ,p_currency_id
      ,p_from_client_id
      ,p_to_client_id
      ,c_payment_status_created
      ,v_message)
    RETURNING payment_id INTO v_payment_id;
    --добавление платежных данных
    payment_detail_api_pack.insert_or_update_payment_detail(p_payment_id     => v_payment_id
                                                           ,p_payment_detail => p_payment_detail);
    disallow_changes();
    RETURN v_payment_id;
  EXCEPTION
    WHEN OTHERS THEN
      disallow_changes();
      RAISE;
  END create_payment;

  --Сброс платежа в "ошибочный статус" с указанием причины
  PROCEDURE fail_payment
  (
    p_payment_id    payment.payment_id%TYPE
   ,p_status_reason payment.status_change_reason%TYPE
   ,p_date          payment.create_dtime%TYPE
  ) IS
  BEGIN
    IF p_payment_id IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_object_id);
    END IF;
    IF p_status_reason IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_reason);
    END IF;
    try_lock_payment(p_payment_id => p_payment_id);
    allow_changes();
    --обновление платежа
    UPDATE payment p
       SET p.status               = c_payment_status_error
          ,p.status_change_reason = p_status_reason
     WHERE p.payment_id = p_payment_id
       AND p.status = c_payment_status_created;
    disallow_changes();
  EXCEPTION
    WHEN OTHERS THEN
      disallow_changes();
      RAISE;
  END fail_payment;

  --Отмена платежа с указанием причины
  PROCEDURE cancel_payment
  (
    p_payment_id    payment.payment_id%TYPE
   ,p_status_reason payment.status_change_reason%TYPE
   ,p_date          payment.create_dtime%TYPE
  ) IS
  BEGIN
    IF p_payment_id IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_object_id);
    END IF;
    IF p_status_reason IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_reason);
    END IF;
    try_lock_payment(p_payment_id => p_payment_id);
    allow_changes();
    --обновление платежа
    UPDATE payment p
       SET p.status               = c_payment_status_canceled
          ,p.status_change_reason = p_status_reason
     WHERE p.payment_id = p_payment_id
       AND p.status = c_payment_status_created;
    disallow_changes();
  EXCEPTION
    WHEN OTHERS THEN
      disallow_changes();
      RAISE;
  END cancel_payment;

  --Перевод платежа в "успешный статус"
  PROCEDURE successful_finish_payment
  (
    p_payment_id payment.payment_id%TYPE
   ,p_date       payment.create_dtime%TYPE
  ) IS
  BEGIN
    IF p_payment_id IS NULL
    THEN
      raise_application_error(common_pack.c_error_code_invalid_input_parameter
                             ,common_pack.c_error_msg_object_id);
    END IF;
    try_lock_payment(p_payment_id => p_payment_id);
    allow_changes();
    --обновление платежа
    UPDATE payment p
       SET p.status = c_payment_status_completed
     WHERE p.payment_id = p_payment_id
       AND p.status = c_payment_status_created;
    disallow_changes();
  EXCEPTION
    WHEN OTHERS THEN
      disallow_changes();
      RAISE;
  END successful_finish_payment;

  --Блокировка платежа для изменения
  PROCEDURE try_lock_payment(p_payment_id payment.payment_id%TYPE) IS
    v_status payment.status%TYPE;
  BEGIN
    SELECT t.status INTO v_status FROM payment t WHERE t.payment_id = p_payment_id FOR UPDATE NOWAIT;
    IF v_status != c_payment_status_created
    THEN
      raise_application_error(common_pack.c_error_code_update_final_status
                             ,common_pack.c_error_msg_update_final_status);
    END IF;
  EXCEPTION
    WHEN no_data_found THEN
      raise_application_error(common_pack.c_error_code_object_not_found
                             ,common_pack.c_error_msg_object_not_found);
    WHEN common_pack.e_row_locked THEN
      raise_application_error(common_pack.c_error_code_object_already_locked
                             ,common_pack.c_error_msg_object_already_locked);
  END try_lock_payment;

  --Проверка, вызываемая из тригера
  PROCEDURE is_changes_through_api IS
  BEGIN
    IF NOT g_is_api
       AND NOT common_pack.is_manual_changes_allowed
    THEN
      raise_application_error(common_pack.c_error_code_manual_changes
                             ,common_pack.c_error_msg_manual_changes);
    END IF;
  END is_changes_through_api;

  PROCEDURE check_payment_delete_restriction IS
  BEGIN
    IF NOT common_pack.is_manual_changes_allowed
    THEN
    
      raise_application_error(common_pack.c_error_code_delete_forbidden
                             ,common_pack.c_error_msg_delete_forbidden);
    END IF;
  END check_payment_delete_restriction;
END;
/