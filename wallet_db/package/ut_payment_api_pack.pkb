CREATE OR REPLACE PACKAGE BODY ut_payment_api_pack IS

  g_payment_id    payment.payment_id%TYPE;
  g_payment_row   payment%ROWTYPE;
  g_status_reason payment.status_change_reason%TYPE;

  PROCEDURE setup IS
  BEGIN
    g_payment_id    := ut_common_pack.create_default_payment;
    g_status_reason := ut_common_pack.get_random_reason;
  END setup;

  PROCEDURE created_payment_with_status_is_created IS
    v_payment_detail t_payment_detail_array;
    v_from_client_id client.client_id%TYPE;
    v_to_client_id   client.client_id%TYPE;
    v_summa          payment.summa%TYPE;
    v_currency_id    currency.currency_id%TYPE;
    v_date           payment.create_dtime%TYPE;
  BEGIN
    --arrange
    v_from_client_id := ut_common_pack.create_default_client;
    v_to_client_id   := ut_common_pack.create_default_client;
    v_payment_detail := ut_common_pack.get_random_payment_detail_array;
    v_summa          := ut_common_pack.get_random_number(p_scale => 2);
    v_currency_id    := ut_common_pack.get_random_currency_id;
    v_date           := ut_common_pack.get_random_timestamp;
    --act
    g_payment_id  := payment_api_pack.create_payment(p_payment_detail => v_payment_detail
                                                    ,p_from_client_id => v_from_client_id
                                                    ,p_to_client_id   => v_to_client_id
                                                    ,p_summa          => v_summa
                                                    ,p_currency_id    => v_currency_id
                                                    ,p_date           => v_date);
    g_payment_row := ut_common_pack.get_payment_info(p_payment_id => g_payment_id);
    --assert                                              
    ut3.ut.expect(g_payment_row.create_dtime
                 ,'Дата создания платежа не соответствует ожидаемому').to_equal(v_date);
    ut3.ut.expect(g_payment_row.summa
                 ,'Сумма платежа не соответствует ожидаемому').to_equal(v_summa);
    ut3.ut.expect(g_payment_row.currency_id
                 ,'ID валюты платежа не соответствует ожидаемому').to_equal(v_currency_id);
    ut3.ut.expect(g_payment_row.from_client_id
                 ,'ID "клиента от" не соответствует ожидаемому').to_equal(v_from_client_id);
    ut3.ut.expect(g_payment_row.to_client_id
                 ,'ID "клиента к" не соответствует ожидаемому').to_equal(v_to_client_id);
    ut3.ut.expect(g_payment_row.status
                 ,'Статус платежа не соответствует ожидаемому').to_equal(payment_api_pack.c_payment_status_created);
    ut3.ut.expect(g_payment_row.status_change_reason
                 ,'Причина изменения платежа не соответствует ожидаемому').to_equal(payment_api_pack.c_payment_create_status_change_reason);
    ut3.ut.expect(g_payment_row.create_dtime_tech
                 ,'Время создания платежа не равно времени изменению').to_equal(g_payment_row.update_dtime_tech);
  END created_payment_with_status_is_created;

  PROCEDURE failed_payment_with_status_is_error IS
  BEGIN
    --act
    payment_api_pack.fail_payment(p_payment_id => g_payment_id, p_status_reason => g_status_reason);
    g_payment_row := ut_common_pack.get_payment_info(p_payment_id => g_payment_id);
    --assert
    ut3.ut.expect(g_payment_row.status
                 ,'Статус платежа не соответствует ожидаемому').to_equal(payment_api_pack.c_payment_status_error);
    ut3.ut.expect(g_payment_row.status_change_reason
                 ,'Причина платежа не соответствует ожидаемому').to_equal(g_status_reason);
    ut3.ut.expect(g_payment_row.create_dtime_tech
                 ,'Время создания платежа равно времени изменению').not_to_equal(g_payment_row.update_dtime_tech);
  END failed_payment_with_status_is_error;

  PROCEDURE canceled_payment_with_status_is_cancel IS
  BEGIN
    --act
    payment_api_pack.cancel_payment(p_payment_id => g_payment_id, p_status_reason => g_status_reason);
    g_payment_row := ut_common_pack.get_payment_info(p_payment_id => g_payment_id);
    --assert
    ut3.ut.expect(g_payment_row.status
                 ,'Статус платежа не соответствует ожидаемому').to_equal(payment_api_pack.c_payment_status_canceled);
    ut3.ut.expect(g_payment_row.status_change_reason
                 ,'Причина платежа не соответствует ожидаемому').to_equal(g_status_reason);
  END canceled_payment_with_status_is_cancel;

  PROCEDURE successful_payment_with_status_is_success IS
  BEGIN
    --act
    payment_api_pack.successful_finish_payment(p_payment_id => g_payment_id);
    g_payment_row := ut_common_pack.get_payment_info(p_payment_id => g_payment_id);
    --assert
    ut3.ut.expect(g_payment_row.status
                 ,'Статус платежа не соответствует ожидаемому').to_equal(payment_api_pack.c_payment_status_completed);
  END successful_payment_with_status_is_success;

  PROCEDURE error_when_create_payment_with_input_invalid_detail IS
    v_payment_detail t_payment_detail_array;
    v_from_client_id client.client_id%TYPE;
    v_to_client_id   client.client_id%TYPE;
    v_summa          payment.summa%TYPE;
    v_currency_id    currency.currency_id%TYPE;
    v_date           payment.create_dtime%TYPE;
  BEGIN
    --arrange
    v_from_client_id := ut_common_pack.create_default_client;
    v_to_client_id   := ut_common_pack.create_default_client;
    v_payment_detail := NULL;
    v_summa          := ut_common_pack.get_random_number(p_scale => 2);
    v_currency_id    := ut_common_pack.get_random_currency_id;
    v_date           := ut_common_pack.get_random_timestamp;
    --act
    g_payment_id := payment_api_pack.create_payment(p_payment_detail => v_payment_detail
                                                   ,p_from_client_id => v_from_client_id
                                                   ,p_to_client_id   => v_to_client_id
                                                   ,p_summa          => v_summa
                                                   ,p_currency_id    => v_currency_id
                                                   ,p_date           => v_date);
  END error_when_create_payment_with_input_invalid_detail;

  PROCEDURE error_when_fail_payment_with_input_invalid_payment_id IS
  BEGIN
    --arrange
    g_payment_id := NULL;
    --act
    payment_api_pack.fail_payment(p_payment_id => g_payment_id, p_status_reason => g_status_reason);
  END error_when_fail_payment_with_input_invalid_payment_id;

  PROCEDURE error_when_cancel_payment_with_input_invalid_status_reason IS
  BEGIN
    --arrange
    g_status_reason := NULL;
    --act
    payment_api_pack.cancel_payment(p_payment_id => g_payment_id, p_status_reason => g_status_reason);
  END error_when_cancel_payment_with_input_invalid_status_reason;

  PROCEDURE error_when_finish_payment_with_input_invalid_payment_id IS
  BEGIN
    --arrange
    g_payment_id := NULL;
    --act                                                 
    payment_api_pack.successful_finish_payment(p_payment_id => g_payment_id);
  END error_when_finish_payment_with_input_invalid_payment_id;

  PROCEDURE error_when_cancel_payment_with_not_exist_id IS
  BEGIN
    --arrange
    g_payment_id := ut_common_pack.get_random_number(p_low => -100, p_high => -1, p_scale => 1);
    --act                                                
    payment_api_pack.cancel_payment(p_payment_id => g_payment_id, p_status_reason => g_status_reason);
  END error_when_cancel_payment_with_not_exist_id;

  PROCEDURE error_when_payment_is_final_status IS
  BEGIN
    --arrange
    payment_api_pack.successful_finish_payment(p_payment_id => g_payment_id);
    --act
    payment_api_pack.successful_finish_payment(p_payment_id => g_payment_id);
  END error_when_payment_is_final_status;

END ut_payment_api_pack;
/