CREATE OR REPLACE PACKAGE client_data_api_pack IS

  /*
  ��������: API ��� ��������� "���������� ������"
  */

  -- �������/���������� ���������� ������
  PROCEDURE insert_or_update_client_data
  (
    p_client_id   client.client_id%TYPE
   ,p_client_data t_client_data_array
  );

  -- �������� ���������� ������
  PROCEDURE delete_client_data
  (
    p_client_id        client.client_id%TYPE
   ,p_delete_field_ids t_number_array
  );

  -- ����������� �� ��������� ����� API
  PROCEDURE is_changes_through_api;

END;
/
