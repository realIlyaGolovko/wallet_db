CREATE OR REPLACE PACKAGE client_api_pack IS
  /*
  ��������: API ��� �������� "������"
  */

  -- ������� ���������� �������
  c_active   CONSTANT client.is_active%TYPE := 1;
  c_inactive CONSTANT client.is_active%TYPE := 0;
  -- ������� ���������� �������
  c_not_blocked CONSTANT client.is_blocked%TYPE := 0;
  c_blocked     CONSTANT client.is_blocked%TYPE := 1;

  ---- API

  -- �������� �������
  FUNCTION create_client(p_client_data t_client_data_array) RETURN client.client_id%TYPE;

  -- ���������� �������
  PROCEDURE block_client
  (
    p_client_id client.client_id%TYPE
   ,p_reason    client.blocked_reason%TYPE
  );

  -- ������������� �������
  PROCEDURE unblock_client(p_client_id client.client_id%TYPE);

  -- ������ �������������
  PROCEDURE deactivate_client(p_client_id client.client_id%TYPE);

  -- ���������� ������� ��� ���������
  PROCEDURE try_lock_client(p_client_id client.client_id%TYPE);

  ---- ��������

  -- ��������, ������������ ��������� �������
  PROCEDURE is_changes_through_api;

  -- ��������, �� ����������� ������� ������
  PROCEDURE check_client_delete_restriction;

END;
/
