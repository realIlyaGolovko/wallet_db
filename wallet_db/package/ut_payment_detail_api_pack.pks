CREATE OR REPLACE PACKAGE ut_payment_detail_api_pack IS

  -- Author  : Головко И.И.
  -- Описание : : Юнит-тесты для функционала "Детали платежа"

  --%suite(Unit-tests for payment_detail)
  --%suitepath(payment_detail)

  --инициализация дефолтного платежа
  PROCEDURE setup_default_payment;

  --%test(Вставка деталей платежа создает новые детали платежа)
  --%tags(positive)
  PROCEDURE insert_payment_detail_create_new_details;

  --%test(Изменение деталей платежа изменяет детали платежа)
  --%tags(positive)
  PROCEDURE update_payment_detail_change_details;

  --%test(Удаления деталей платежа удаляет детали платежа)
  --%tags(positive)
  PROCEDURE delete_payment_detail_delete_details;

  --%test(Ошибка при вставке/изменении деталей платежа с невалидными деталями платежа)
  --%beforetest(setup_default_payment)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE error_when_insert_or_update_detail_with_input_invalid_details;

  --%test(Ошибка при удалении деталей платежа с невалдиным id платежа)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE error_when_delete_payment_detail_with_input_invalid_payment_id;

  --%test(Ошибка при удалении деталей платежа c непроинициализированной входной коллекцией)
  --%beforetest(setup_default_payment)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE erro_when_delete_payment_detail_with_input_not_init_collection;

  --%test(Ошибка при удалении деталей платежа c пустой  входной коллекцией)
  --%beforetest(setup_default_payment)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE erro_when_delete_payment_detail_with_input_empty_collection;

END ut_payment_detail_api_pack;
/