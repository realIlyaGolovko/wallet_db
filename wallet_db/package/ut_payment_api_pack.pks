CREATE OR REPLACE PACKAGE ut_payment_api_pack IS

  -- Author  : Головко И.И.
  -- Описание : Юнит-тесты для функционала "Платеж"

  --%suite(Unit-tests for payment)
  --%suitepath(payment)

  PROCEDURE setup;

  --%context(create_payment)
  --%displayname(Создание платежа)

  --%test(Создание платежа)
  --%tags(positive)
  PROCEDURE created_payment_with_status_is_created;

  --%test(Ошибка при создании платежа с невалидными деталями платежа)
  --%throws(common_pack.e_invalid_input_parameter) 
  PROCEDURE error_when_create_payment_with_input_invalid_detail;

  --%endcontext

  --%context(change_payment)
  --%displayname(Изменение платежа)
  --%beforeeach(setup)

  --%test(Cброс платежа в "ошибочный статус")
  --%tags(positive)
  PROCEDURE failed_payment_with_status_is_error;

  --%test(Отмены платежа)
  --%tags(positive)
  PROCEDURE canceled_payment_with_status_is_cancel;

  --%test (Перевода платежа в "успешный статус")
  --%tags(positive)
  PROCEDURE successful_payment_with_status_is_success;

  --%test(Ошибка при сбросе платежа в "ошибочный статус" с невалидным id платежа)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE error_when_fail_payment_with_input_invalid_payment_id;

  --%test(Ошибка при отмене платежа с с невалидной причиной отмены)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE error_when_cancel_payment_with_input_invalid_status_reason;

  --%test(Ошибка при переводе платежа в "успешный статус" с невалидным id платежа)
  --%throws(common_pack.e_invalid_input_parameter)
  PROCEDURE error_when_finish_payment_with_input_invalid_payment_id;

  --%test(Ошибка при отсутствии в таблице payment переданного ID платежа)
  --%throws(common_pack.e_object_not_found)
  PROCEDURE error_when_cancel_payment_with_not_exist_id;

  --%test(Ошибка при изменении платежа в финальном статусе)
  --%throws(common_pack.e_update_final_status) 
  PROCEDURE error_when_payment_is_final_status;

--%endcontext

END ut_payment_api_pack;
/