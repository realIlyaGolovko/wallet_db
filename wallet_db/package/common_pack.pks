CREATE OR REPLACE PACKAGE common_pack IS

  --Автор: Головко И.И.
  -- Purpose : Общие объекты

  c_error_msg_field_id              CONSTANT VARCHAR2(100 CHAR) := 'ID поля не может быть пустым';
  c_error_msg_filed_value           CONSTANT VARCHAR2(100 CHAR) := 'Значение в поле не может быть пустым';
  c_error_msg_empty_collection      CONSTANT VARCHAR2(100 CHAR) := 'Коллекция не содержит данных';
  c_error_msg_object_id             CONSTANT VARCHAR2(100 CHAR) := 'ID объекта не может быть пустым';
  c_error_msg_reason                CONSTANT VARCHAR2(100 CHAR) := 'Причина не может быть пустой';
  c_error_msg_delete_forbidden      CONSTANT VARCHAR2(100 CHAR) := 'Удаление объекта запрещено';
  c_error_msg_manual_changes        CONSTANT VARCHAR2(100 CHAR) := 'Изменения должны выполнять только через API';
  c_error_msg_collection_is_empty   CONSTANT VARCHAR2(100 CHAR) := 'Коллекция не содержит данных';
  c_error_msg_update_final_status   CONSTANT VARCHAR2(100 CHAR) := 'Объект в конечном статусе. Изменения невозможны';
  c_error_msg_object_not_found      CONSTANT VARCHAR2(100 CHAR) := 'Объект не найден';
  c_error_msg_object_already_locked CONSTANT VARCHAR2(100 CHAR) := 'Объект уже заблокирован';

  c_error_code_invalid_input_parameter CONSTANT NUMBER(10) := -20101;
  c_error_code_delete_forbidden        CONSTANT NUMBER(10) := -20102;
  c_error_code_manual_changes          CONSTANT NUMBER(10) := -20103;
  c_error_code_update_final_status     CONSTANT NUMBER(10) := -20104;
  c_error_code_object_not_found        CONSTANT NUMBER(10) := -20105;
  c_error_code_object_already_locked   CONSTANT NUMBER(10) := -20106;

  e_invalid_input_parameter EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_invalid_input_parameter, c_error_code_invalid_input_parameter);
  e_delete_forbidden EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_delete_forbidden, c_error_code_delete_forbidden);
  e_manual_changes EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_manual_changes, c_error_code_manual_changes);
  e_update_final_status EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_update_final_status, c_error_code_update_final_status);
  e_object_not_found EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_object_not_found, c_error_code_object_not_found);
  e_row_locked EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_row_locked, -0054);
  e_object_already_locked EXCEPTION;
  PRAGMA EXCEPTION_INIT(e_object_already_locked, c_error_code_object_already_locked);

  --Включение/отключение разрешения менять вручную данные объектов
  PROCEDURE enable_manual_changes;
  PROCEDURE disable_manual_changes;
  --Разрешены ли ручные изменения на глобальном уровне
  FUNCTION is_manual_changes_allowed RETURN BOOLEAN;

END common_pack;
/