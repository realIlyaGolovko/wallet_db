CREATE OR REPLACE PACKAGE BODY ut_common_pack IS

  -- Генерация значения полей для сущности клиент
  FUNCTION get_random_client_email RETURN client_data.field_value%TYPE IS
  BEGIN
    RETURN dbms_random.string('l', 10) || '@' || dbms_random.string('l', 10) || '.com';
  END;

  FUNCTION get_random_client_mobile_phone RETURN client_data.field_value%TYPE IS
  BEGIN
    RETURN '+7' || trunc(dbms_random.value(79000000000, 79999999999));
  END;

  FUNCTION get_random_client_inn RETURN client_data.field_value%TYPE IS
  BEGIN
    RETURN trunc(dbms_random.value(1000000000000, 99999999999999));
  END;

  FUNCTION get_random_client_bday RETURN client_data.field_value%TYPE IS
  BEGIN
    RETURN ADD_MONTHS(trunc(SYSDATE), -trunc(dbms_random.value(18 * 12, 50 * 12)));
  END;

  FUNCTION create_default_client(p_client_data t_client_data_array := NULL) RETURN client.client_id%TYPE IS
    v_client_data t_client_data_array := p_client_data;
  BEGIN
    -- если ничего не передано, то по умолчанию генерим какие-то значения
    IF v_client_data IS NULL
       OR v_client_data IS empty
    THEN
      v_client_data := t_client_data_array(t_client_data(c_client_field_email_id
                                                        ,get_random_client_email())
                                          ,t_client_data(c_client_mobile_phone_id
                                                        ,get_random_client_mobile_phone())
                                          ,t_client_data(c_client_inn_id, get_random_client_inn())
                                          ,t_client_data(c_client_birthday_id
                                                        ,get_random_client_bday()));
    END IF;
  
    RETURN client_api_pack.create_client(p_client_data => v_client_data);
  END;

  PROCEDURE ut_failed IS
  BEGIN
    raise_application_error(c_error_code_test_failed, c_error_msg_test_failed);
  END ut_failed;

  FUNCTION get_random_number
  (
    p_low   IN NUMBER DEFAULT 0
   ,p_high  IN NUMBER DEFAULT 1000000
   ,p_scale IN NUMBER DEFAULT 4
  ) RETURN NUMBER IS
  BEGIN
    RETURN ROUND(dbms_random.value(p_low, p_high), p_scale);
  END get_random_number;

  FUNCTION get_random_timestamp RETURN payment.create_dtime%TYPE IS
    v_difference  NUMBER := get_random_number(p_low => -86400, p_high => 86400, p_scale => 4);
    v_secs_in_day NUMBER := 24 * 60 * 60;
  BEGIN
    RETURN trunc(systimestamp) + v_difference / v_secs_in_day;
  END get_random_timestamp;

  FUNCTION get_random_reason
  (
    p_option IN CHAR DEFAULT NULL
   ,p_length IN NUMBER DEFAULT NULL
  ) RETURN payment.status_change_reason%TYPE IS
    v_option CHAR;
    v_length NUMBER;
    v_reason payment.status_change_reason%TYPE;
  BEGIN
    v_option := coalesce(p_option, 'P');
    v_length := coalesce(p_length, 50);
    v_reason := dbms_random.string(opt => v_option, len => v_length);
    RETURN v_reason;
  END get_random_reason;

  FUNCTION get_random_currency_id RETURN currency.currency_id%TYPE IS
    v_currency_id currency.currency_id%TYPE;
  BEGIN
    SELECT c.currency_id
      INTO v_currency_id
      FROM currency c
     ORDER BY dbms_random.value
     FETCH FIRST 1 rows ONLY;
    RETURN v_currency_id;
  END get_random_currency_id;

  FUNCTION get_random_payment_detail
  (
    p_field_id    IN payment_detail.field_id%TYPE DEFAULT NULL
   ,p_field_value IN payment_detail.field_value%TYPE DEFAULT NULL
  ) RETURN t_payment_detail IS
    v_payment_detail t_payment_detail;
    v_field_id       payment_detail_field.field_id%TYPE;
    v_field_value    payment_detail.field_value%TYPE;
  BEGIN
    IF p_field_id IS NULL
    THEN
      SELECT f.field_id
        INTO v_field_id
        FROM payment_detail_field f
       ORDER BY dbms_random.value
       FETCH FIRST 1 rows ONLY;
    ELSE
      v_field_id := p_field_id;
    END IF;
    v_field_value    := coalesce(p_field_value, dbms_random.string('P', 50));
    v_payment_detail := t_payment_detail(v_field_id, v_field_value);
    RETURN v_payment_detail;
  END get_random_payment_detail;

  FUNCTION get_random_payment_detail_array(p_detail_amount IN NUMBER DEFAULT NULL)
    RETURN t_payment_detail_array IS
    v_payment_detail       t_payment_detail;
    v_payment_detail_array t_payment_detail_array := t_payment_detail_array();
    v_field_max_count      NUMBER;
    v_spot                 NUMBER := 1;
  BEGIN
    SELECT COUNT(*) INTO v_field_max_count FROM payment_detail_field;
    IF p_detail_amount > v_field_max_count
    THEN
      RAISE common_pack.e_invalid_input_parameter;
    END IF;
    IF p_detail_amount IS NULL
    THEN
      FOR counter IN (SELECT *
                        FROM (SELECT f.field_id FROM payment_detail_field f ORDER BY dbms_random.value)
                       WHERE rownum <= get_random_number(1, v_field_max_count, 1))
      LOOP
        v_payment_detail_array.extend(1);
        v_payment_detail := get_random_payment_detail(p_field_id => counter.field_id);
        v_payment_detail_array(v_spot) := v_payment_detail;
        v_spot := v_spot + 1;
      END LOOP;
    ELSE
      FOR counter IN (SELECT *
                        FROM (SELECT f.field_id FROM payment_detail_field f ORDER BY dbms_random.value)
                       WHERE rownum <= p_detail_amount)
      LOOP
        v_payment_detail_array.extend(1);
        v_payment_detail := get_random_payment_detail(p_field_id => counter.field_id);
        v_payment_detail_array(v_spot) := v_payment_detail;
        v_spot := v_spot + 1;
      END LOOP;
    END IF;
    RETURN v_payment_detail_array;
  EXCEPTION
    WHEN common_pack.e_invalid_input_parameter THEN
      raise_application_error(common_pack.c_error_msg_object_not_found
                             ,common_pack.c_error_code_invalid_input_parameter);
  END get_random_payment_detail_array;

  FUNCTION create_default_payment
  (
    p_payment_detail IN t_payment_detail_array DEFAULT NULL
   ,p_from_client_id IN client.client_id%TYPE DEFAULT NULL
   ,p_to_client_id   IN client.client_id%TYPE DEFAULT NULL
   ,p_summa          IN payment.summa%TYPE DEFAULT NULL
   ,p_currency_id    IN payment.currency_id%TYPE DEFAULT NULL
   ,p_date           IN payment.create_dtime%TYPE DEFAULT NULL
  ) RETURN payment.payment_id%TYPE IS
    v_payment_detail t_payment_detail_array;
    v_from_client_id client.client_id%TYPE;
    v_to_client_id   client.client_id%TYPE;
    v_summa          payment.summa%TYPE;
    v_currency_id    payment.currency_id%TYPE;
    v_date           payment.create_dtime%TYPE;
    v_payment_id     payment.payment_id%TYPE;
  BEGIN
    IF p_payment_detail IS NULL
       OR p_payment_detail IS empty
    THEN
      v_payment_detail := get_random_payment_detail_array;
    ELSE
      v_payment_detail := p_payment_detail;
    END IF;
  
    v_from_client_id := coalesce(p_from_client_id, ut_common_pack.create_default_client);
    v_to_client_id   := coalesce(p_to_client_id, ut_common_pack.create_default_client);
    v_summa          := coalesce(p_summa, get_random_number(p_low => 1, p_scale => 2));
    v_currency_id    := coalesce(p_currency_id, get_random_currency_id);
    v_date           := coalesce(p_date, get_random_timestamp);
  
    v_payment_id := payment_api_pack.create_payment(p_payment_detail => v_payment_detail
                                                   ,p_from_client_id => v_from_client_id
                                                   ,p_to_client_id   => v_to_client_id
                                                   ,p_summa          => v_summa
                                                   ,p_currency_id    => v_currency_id
                                                   ,p_date           => v_date);
    RETURN v_payment_id;
  
  END create_default_payment;

  FUNCTION get_payment_info(p_payment_id payment.payment_id%TYPE) RETURN payment%ROWTYPE IS
    v_payment_row payment%ROWTYPE;
  BEGIN
    SELECT * INTO v_payment_row FROM payment t WHERE t.payment_id = p_payment_id;
    RETURN v_payment_row;
  END get_payment_info;

  --Возвращает информацию по полю платежа
  FUNCTION get_payment_info_field_value
  (
    p_payment_id IN payment_detail.payment_id%TYPE
   ,p_field_id   payment_detail.field_id%TYPE
  ) RETURN payment_detail.field_value%TYPE IS
    v_field_value payment_detail.field_value%TYPE;
  BEGIN
    SELECT MAX(pd.field_value)
      INTO v_field_value
      FROM payment_detail pd
     WHERE pd.payment_id = p_payment_id
       AND pd.field_id = p_field_id;
    RETURN v_field_value;
  END get_payment_info_field_value;

END ut_common_pack;
/