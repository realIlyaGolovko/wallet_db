CREATE OR REPLACE PACKAGE ut_common_pack IS

  -- Author  : Головко И.И.
  -- Описание : Вспомогательный пакет для юнит-тестов

  c_client_field_email_id  CONSTANT client_data_field.field_id%TYPE := 1;
  c_client_mobile_phone_id CONSTANT client_data_field.field_id%TYPE := 2;
  c_client_inn_id          CONSTANT client_data_field.field_id%TYPE := 3;
  c_client_birthday_id     CONSTANT client_data_field.field_id%TYPE := 4;

  c_client_status_is_active  CONSTANT client.is_active%TYPE := 1;
  c_client_status_is_blocked CONSTANT client.is_blocked%TYPE := 0;

  c_error_code_test_failed CONSTANT NUMBER(10) := -20999;
  c_error_msg_test_failed  CONSTANT VARCHAR2(100 CHAR) := 'Unit-тест не прошел';

  -- Генерация значения полей для сущности клиент
  FUNCTION get_random_client_email RETURN client_data.field_value%TYPE;
  FUNCTION get_random_client_mobile_phone RETURN client_data.field_value%TYPE;
  FUNCTION get_random_client_inn RETURN client_data.field_value%TYPE;
  FUNCTION get_random_client_bday RETURN client_data.field_value%TYPE;
  -- Создание клиента
  FUNCTION create_default_client(p_client_data t_client_data_array := NULL) RETURN client.client_id%TYPE;

  -- Возбуждение исключения о неверном тесте
  PROCEDURE ut_failed;

  --Генерирует случайное число
  FUNCTION get_random_number
  (
    p_low   IN NUMBER DEFAULT 0
   ,p_high  IN NUMBER DEFAULT 1000000
   ,p_scale IN NUMBER DEFAULT 4
  ) RETURN NUMBER;

  --Генерирует случайную дату
  FUNCTION get_random_timestamp RETURN payment.create_dtime%TYPE;

  --Генерирует случайную причину
  FUNCTION get_random_reason
  (
    p_option IN CHAR DEFAULT NULL
   ,p_length IN NUMBER DEFAULT NULL
  ) RETURN payment.status_change_reason%TYPE;

  --Генерирует id случайной валютной пары
  FUNCTION get_random_currency_id RETURN currency.currency_id%TYPE;

  --Генерирует случайные платежные данные
  FUNCTION get_random_payment_detail
  (
    p_field_id    IN payment_detail.field_id%TYPE DEFAULT NULL
   ,p_field_value IN payment_detail.field_value%TYPE DEFAULT NULL
  ) RETURN t_payment_detail;

  --Генерирует случайную коллекцию платежных данных
  FUNCTION get_random_payment_detail_array(p_detail_amount IN NUMBER DEFAULT NULL)
    RETURN t_payment_detail_array;

  --Генерирует случаный платеж
  FUNCTION create_default_payment
  (
    p_payment_detail IN t_payment_detail_array DEFAULT NULL
   ,p_from_client_id IN client.client_id%TYPE DEFAULT NULL
   ,p_to_client_id   IN client.client_id%TYPE DEFAULT NULL
   ,p_summa          IN payment.summa%TYPE DEFAULT NULL
   ,p_currency_id    IN payment.currency_id%TYPE DEFAULT NULL
   ,p_date           IN payment.create_dtime%TYPE DEFAULT NULL
  ) RETURN payment.payment_id%TYPE;

  --Возвращает информацию о сущности "Платеж"
  FUNCTION get_payment_info(p_payment_id payment.payment_id%TYPE) RETURN payment%ROWTYPE;

  --Возвращает информацию по полю платежа
  FUNCTION get_payment_info_field_value
  (
    p_payment_id IN payment_detail.payment_id%TYPE
   ,p_field_id   payment_detail.field_id%TYPE
  ) RETURN payment_detail.field_value%TYPE;

END ut_common_pack;
/