CREATE OR REPLACE PACKAGE BODY common_pack IS
  g_enable_manual_changes BOOLEAN := FALSE; --разрешены ли изменения объектов не через API

  PROCEDURE enable_manual_changes IS
  BEGIN
    g_enable_manual_changes := TRUE;
  END enable_manual_changes;

  PROCEDURE disable_manual_changes IS
  BEGIN
    g_enable_manual_changes := FALSE;
  END disable_manual_changes;

  FUNCTION is_manual_changes_allowed RETURN BOOLEAN IS
  BEGIN
    RETURN g_enable_manual_changes;
  END is_manual_changes_allowed;

END common_pack;
/