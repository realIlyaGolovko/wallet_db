--c����� ��������� �������� � �����
spool install_objectsq.log
spool on

prompt ======================================================
prompt ===== INSTALL OBJECTS
prompt ======================================================

prompt ==== Drop objects...
@@drop_objects.sql


prompt ==== Create tables...
@@client.tbl
@@client_data_filed.tbl
@@client_data.tbl
@@currency.tbl
@@payment.tbl
@@payment_detail_field.tbl
@@payment_detail.tbl
@@wallet.tbl
@@account.tbl


prompt ==== Create sequenses...
@@client_seq.seq
@@payment_seq.seq
@@account_seq.seq
@@wallet_seq.seq

prompt ==== Insert data...
@@insert_currency.sql
@@insert_client_data_field.sql
@@insert_payment_detail_field.sql

prompt ==== Types...
@@t_client_data.tps
@@t_client_data_array.tps
@@t_number_array.tps
@@t_payment_detail.tps
@@t_payment_detail_array.tps

prompt ==== Packages...
@@common_pack.pks
@@client_data_api_pack.pks
@@client_api_pack.pks
@@payment_api_pack.pks
@@payment_data_api_pack.pks


prompt ==== Package bodies...
@@common_pack.pkb
@@client_api_pack.pkb
@@client_data_api_pack.pkb
@@payment_api_pack.pkb
@@payment_data_api_pack.pkb

prompt ==== Triggers...
@@client_b_d_restrict.trg
@@client_b_iu_api.trg
@@client_b_iu_tech_fields.trg
@@client_data_b_iud_api.trg
@@payment_b_d_restrict.trg
@@payment_b_iu_api.trg
@@payment_b_iu_tech_fields.trg
@@payment_detail_b_iud_api.trg

prompt ==== Ut objects...

@@ut_common_pack.pks
@@ut_payment_api_pack.pks
@@ut_payment_detail_api_pack.pks



@@ut_common_pack.pkb
@@ut_payment_api_pack.pkb
@@ut_payment_detail_api_pack.pkb

