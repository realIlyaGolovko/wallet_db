BEGIN
  EXECUTE IMMEDIATE 'drop table client_data';
  EXECUTE IMMEDIATE 'drop table client_data_field';
  EXECUTE IMMEDIATE 'drop table payment_detail_field';
  EXECUTE IMMEDIATE 'drop table payment';
  EXECUTE IMMEDIATE 'drop table client';
  EXECUTE IMMEDIATE 'drop table currency';
  EXECUTE IMMEDIATE 'drop sequence client_seq';
  EXECUTE IMMEDIATE 'drop sequence payment_seq';
  EXECUTE IMMEDIATE 'drop table account';
  EXECUTE IMMEDIATE 'drop table wallet';
  EXECUTE IMMEDIATE 'drop sequence wallet_seq';
  EXECUTE IMMEDIATE 'drop sequence account_seq';
EXCEPTION
  WHEN OTHERS THEN
    NULL;
END;
/
